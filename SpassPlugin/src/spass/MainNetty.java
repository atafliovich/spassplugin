package spass;
import java.io.BufferedReader;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import plugins.NettyPlugin;
import plugins.PluginAPI;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.justification.Justification;

/**
 * 
 */

/**
 * @author ben
 *
 */
public class MainNetty implements NettyPlugin{
	//has the user been warned about equalities
	public static boolean warned = false;
	
	//contains translation from netty symbols to spass functions.
	public static final HashMap<String,String> functions;
	public static final HashMap<String,Integer> arity;
	public static final HashMap<String,String> quantifiers;
	static {
		functions = new HashMap<String,String>();
		functions.put("∧", "and");
		functions.put("∨", "or");
		functions.put("⇒", "implies");
		functions.put("¬", "not");
		functions.put("⊤", "true");
		functions.put("⊥", "false");
		
		arity = new HashMap<String, Integer>();
		arity.put("∧", 2);
		arity.put("∨", 2);
		arity.put("⇒", 2);
		arity.put("¬", 1);
		
		quantifiers = new HashMap<String,String>();
		quantifiers.put("∀", "forall");
		quantifiers.put("∃", "exists");
	}

	/**
	 * runs if the user tries running the jar by itself
	 */
	public static void main(String[] args) {
		System.out.println("Please install this plugin by opening Netty and adding this file to the plugin tab.");
	}

	public String getDescription() {
		return "Solve with SPASS.";
	}

	public Icon getIcon() {
		return null;
	}

	public String getPluginName() {
		return "SPASS";
	}
	
	/**
	 * @param expr
	 * @return true if the expression contains a quantifer
	 */
	private boolean isQuant(Expression expr) {
		if (expr instanceof Application) {
			Application appr = (Application) expr;
			if (quantifiers.containsKey(appr.getFunction().toString())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param expr
	 * @return true if expr is a boolean (eg. top or bot)
	 */
	private boolean isBool(Expression expr) {
		return (expr.toString().equals("⊤") || expr.toString().equals("⊥"));
	}
	
	private String extractQuantVars(Expression expr) {
		String raw = expr.getChild(0).toString();
		//isolating variables
		return raw.substring(1, raw.indexOf(':'));

	}
	
	private String buildConjectureHelper(Expression rawexpr) {
		//builds the final conjecture for SPASS
		if (isQuant(rawexpr)) {
			Application appr = (Application) rawexpr;
			//getting the expression that is being quantified
			Expression expr = rawexpr.getChild(0).getChild(1);
			return quantifiers.get(appr.getFunction().toString()) + "([" + extractQuantVars(rawexpr) + "]," + buildConjectureHelper(expr) + ")";
		}
		if (rawexpr instanceof Application) {
			Application appr = (Application) rawexpr;
			String func = appr.getFunction().toString();
			if (functions.containsKey(func)) {
				String result = functions.get(func) + "(" + buildConjectureHelper(appr.getChild(0));
				for (int i=1; i < arity.get(func); i++) {
					result += "," + buildConjectureHelper(appr.getChild(i));
				}
				return result + ")";
			}
		} else if (functions.containsKey(rawexpr.toString())) {
			return functions.get(rawexpr.toString());
		}
		return "booly(" + rawexpr.toString() + ")";
	}
	
	//this function adds extra overhead depending on wether or not there are quantifiers
	private String buildConjecture(Expression expr, Expression expr2) {
		//two boolean expressions
		if (!isQuant(expr) && !isQuant(expr2)) {
			return "forall(" + buildVariables(expr,expr2) + ",equiv(" + buildConjectureHelper(expr) + "," + buildConjectureHelper(expr2) + "))";
		} else {
		//one of them is a quantifier
			return "equiv(" + buildConjectureHelper(expr) + "," + buildConjectureHelper(expr2) + ")"; 
		}
	}
	
	/**
	 * @param expr
	 * @param expr2
	 * @return a string containing all the variables within the expressions
	 */
	private String buildVariables(Expression expr, Expression expr2) {
		//builds the variables for SPASS
		HashSet<Variable> vars = new HashSet<>();
		vars.addAll(expr.getAllVars());
		if (expr2 != null)
			vars.addAll(expr2.getAllVars());
		
		String result = "[";
		for (Variable sym : vars) {
			if (!functions.containsKey(sym.toString()) && !quantifiers.containsKey(sym.toString())) {
				result += sym + ",";
			}
		}
		if (result.endsWith(",")) {
			result = result.substring(0, result.length()-1);
		}
		return result + "]";
		
	}
	
	/**
	 * 
	 * @param expr
	 * @param expr2
	 * @return 1 if proved, 0 if false, or 2 if there was an error.
	 */
	private int callSpass(Expression expr, Expression expr2) {
		//adding the proof overhead
		StringBuilder builder = new StringBuilder();
		builder.append("begin_problem(Netty).\n");
		builder.append("list_of_descriptions.\n");
		builder.append("name({**}).\n");		
		builder.append("author({**}).\n");
		builder.append("status(unsatisfiable).\n");
		builder.append("description({**}).\n");
		builder.append("end_of_list.\n");
		builder.append("list_of_symbols.\n");
		builder.append("predicates[(booly,1)].\n");
		builder.append("end_of_list.\n");
		builder.append("list_of_formulae(axioms).\n");
		builder.append("formula(exists([netty],booly(netty))).\n");
		builder.append("formula(exists([netty],not(booly(netty)))).\n");
		builder.append("end_of_list.\n");
		builder.append("list_of_formulae(conjectures).\n");
		builder.append("formula(" + buildConjecture(expr,expr2) + ").\n");
		builder.append("end_of_list.\n");
		builder.append("end_problem.\n");
		
		String cmd1 = "SPASS";
		
		try {
			//writing the spass file
			PrintWriter spassprove = new PrintWriter("spassproofnetty.txt");
			spassprove.print(builder.toString());
			spassprove.close();
			
			//creating a process
			ProcessBuilder pb = new ProcessBuilder(cmd1, "spassproofnetty.txt");
	
			//getting output of SPASS
			Process p = pb.start();
		    InputStream is = p.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    
		    
		    String line = null;
		    int proved = 2;
		    while ((line = br.readLine()) != null) {
		    	//checking the SPASS results
		    	if (line.endsWith("Proof found.")) {
		    		proved = 1;
		    	}
		    	if (line.endsWith("Completion found.")) {
		    		proved = 0;
		    	}
		    }
		    //waiting for process to finish
		    p.waitFor();
		    
		    //remove after done
		    File output = new File("spassproofnetty.txt");
		    if (proved != 2)
		    	Files.delete(output.toPath());
		    
		    return proved;
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 2;
	}

	//runs when the SPASS button is clicked
	public void run() {
		PluginAPI.loadLines();
		if (PluginAPI.getNumLines() < 3) {
			//not enough lines to prove something
			JOptionPane.showMessageDialog(PluginAPI.prooftree, "Need more lines for SPASS to work.");
			return;
		}
		
		//getting proof lines
		Expression lastline = PluginAPI.getLastLine().getLastLine().getExpn();
		Expression secondlastline = PluginAPI.getSecondLastLine().getLastLine().getExpn();
		
		//warning the user about proving two quantified expressions are equivalent
		//SPASS can handle this though
		if (isQuant(secondlastline) && !isBool(lastline) && !warned) {
			JOptionPane.showMessageDialog(PluginAPI.prooftree, "Warning!  SPASS will only prove that either these lines are both true or both false.");
			warned = true;
		} else if (isQuant(lastline) && !isBool(secondlastline) && !warned) {
			JOptionPane.showMessageDialog(PluginAPI.prooftree, "Warning!  SPASS will only prove that either these lines are both true or both false.");
			warned = true;
		}
		
		int proved = callSpass(lastline, secondlastline);
		if (proved == 1) {
			//conjecture is true
			PluginAPI.getSecondLastLine().setJustification(new Justification("proved by SPASS", true));
			PluginAPI.refreshLastLines();
		} else if (proved == 0) {
			//their claim is false
			JOptionPane.showMessageDialog(PluginAPI.prooftree, "SPASS found a counter example.");
		} else {
			//there was a problem
			JOptionPane.showMessageDialog(PluginAPI.prooftree, "SPASS encountered an error proving your conjecture.");
		}
		
	}

}
