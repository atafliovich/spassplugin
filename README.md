SPASS Plugin for Netty
======================

## Installation

Before installing this plugin, you must first install the classic SPASS
theorem prover from:
[*http://www.mpi-inf.mpg.de/departments/automation-of-logic/software/spass-workbench/classic-spass-theorem-prover/download/*](http://www.mpi-inf.mpg.de/departments/automation-of-logic/software/spass-workbench/classic-spass-theorem-prover/download/)

After installing the plugin, add the main SPASS binary to your PATH
variable:

-   For UNIX-like : Add ***PATH=\$PATH:<insert path to SPASS>***
    to your .bashrc

-   For Windows: My Computer > Properties > Advanced >
    Environment Variables and add the SPASS executable

Then compile the SPASS plugin using ANT.

After compiling with ANT, there should be a new **spass.jar** file in
the directory.

Start **netty.jar** and click on the plugins tab and add **spass.jar**.
![spass1.png](https://bitbucket.org/repo/ga8jpE/images/4275789122-spass1.png)
![spass2.png](https://bitbucket.org/repo/ga8jpE/images/3384745595-spass2.png)

After **restarting Netty**, you should see a new button labelled “SPASS” on
the main toolbar of you proof.

![spass3.png](https://bitbucket.org/repo/ga8jpE/images/3092538607-spass3.png)

For instructions on how to use SPASS, please see our [manual](https://bitbucket.org/atafliovich/spassplugin/wiki/Home).